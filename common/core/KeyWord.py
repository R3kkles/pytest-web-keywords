import time

import allure
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait

from common.core.AssertUtils import AssertUtils

global driver


# selenium读取页面元素的方法封装
def _find_element(find_type, find_key):
    try:
        # 显示等待WebDriverWait
        return WebDriverWait(driver, 3).until(lambda d: d.find_element(by=find_type, value=find_key))
    except Exception as e:
        raise e


class KeyWord:
    STYLE = "background-color: rgba(39,249,143,0.5);"

    # 打开浏览器
    @classmethod
    @allure.step("打开浏览器")
    def open_browser(cls, bowser_name, *args):
        global driver
        option = getattr(webdriver, f"{bowser_name}Options")()
        # 浏览器常驻
        option.add_experimental_option('useAutomationExtension', False)
        option.add_experimental_option('excludeSwitches', ['enable-automation'])
        option.add_experimental_option("detach", True)
        driver = (getattr(webdriver, bowser_name)(options=option))

    @classmethod
    @allure.step("窗口最大化")
    # 窗口最大化
    def max_window(cls, *args):
        driver.maximize_window()

    @classmethod
    # 打开网址
    @allure.step("打开网址")
    def get_url(cls, url, *args):
        driver.get(url)

    # 高亮
    @classmethod
    # @allure.step("文本高亮")
    def high_light(cls, element, *args):
        driver.execute_script("arguments[0].setAttribute('style', arguments[1]);", element, cls.STYLE)

    @classmethod
    @allure.step("{title}")
    # 输入数据
    def input_content(cls, location_type, location_name, content, title):
        element = _find_element(location_type, location_name)
        cls.high_light(element)
        element.send_keys(content)

    @classmethod
    @allure.step("{title}")
    # 点击操作
    def click(cls, location_type, location_name, title):
        element = _find_element(location_type, location_name)
        cls.high_light(element)
        element.click()

    # 切换窗口
    @classmethod
    def switch_frame(cls, location_type, location_name, *args):
        try:
            i_frame = _find_element(location_type, location_name)
            driver.switch_to.frame(i_frame)
        except Exception as e:
            print(e)

    @classmethod
    @allure.step("关闭浏览器")
    def close_browser(cls, *args):
        driver.quit()

    @classmethod
    @allure.step("等待 {seconds} 秒")
    def sleep(cls, seconds, *args):
        time.sleep(seconds)

    @classmethod
    @allure.step("断言")
    def assert_func(cls, location_type, location_name, assert_func, assert_value, *args):
        assert_utils = AssertUtils()
        func = getattr(assert_utils, assert_func)
        element = _find_element(location_type, location_name)
        allure.attach(driver.get_screenshot_as_png(), "截图", allure.attachment_type.PNG)
        func(element.text, assert_value)
