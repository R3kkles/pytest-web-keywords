from common.db.MysqlDB import MysqlDB


class AssertUtils:
    # 大于
    @classmethod
    def assert_gr(cls, element_text, except_value):
        assert element_text > except_value

    # 小于
    @classmethod
    def assert_le(cls, element_text, except_value):
        assert element_text < except_value

    # 大于等于
    @classmethod
    def assert_gr_eq(cls, element_text, except_value):
        assert element_text >= except_value

    # 小于等于
    @classmethod
    def assert_le_eq(cls, element_text, except_value):
        assert element_text <= except_value

    # 等于
    @classmethod
    def assert_eq(cls, element_text, except_value):
        assert element_text == except_value

    # 不等于
    @classmethod
    def assert_not_eq(cls, element_text, except_value):
        assert element_text != except_value

    # 包含
    @classmethod
    def assert_contains(cls, element_text, except_value):
        assert except_value in element_text

    # 不包含
    @classmethod
    def assert_not_contains(cls, element_text, except_value):
        assert except_value not in element_text

    # 布尔判断
    @classmethod
    def assert_is(cls, element_text, except_value):
        assert element_text is except_value

    # 大于
    @classmethod
    def assert_mysql_gr(cls, element_text, except_value):
        mysql_db_ = MysqlDB()
        assert element_text > mysql_db_.fetch_one(except_value)

    # 小于
    @classmethod
    def assert_mysql_le(cls, element_text, except_value):
        mysql_db_ = MysqlDB()
        assert element_text < mysql_db_.fetch_one(except_value)

    # 大于等于
    @classmethod
    def assert_mysql_gr_eq(cls, element_text, except_value):
        mysql_db_ = MysqlDB()
        assert element_text >= mysql_db_.fetch_one(except_value)

    # 小于等于
    @classmethod
    def assert_mysql_le_eq(cls, element_text, except_value):
        mysql_db_ = MysqlDB()
        assert element_text <= mysql_db_.fetch_one(except_value)

    # 等于
    @classmethod
    def assert_mysql_eq(cls, element_text, except_value):
        mysql_db_ = MysqlDB()
        temp = mysql_db_.fetch_one(except_value)
        if type(except_value) != type(temp):
            assert str(element_text) == str(temp)
            return
        assert element_text == temp

    # 不等于
    @classmethod
    def assert_mysql_not_eq(cls, element_text, except_value):
        mysql_db_ = MysqlDB()
        assert element_text != mysql_db_.fetch_one(except_value)

    # 包含
    @classmethod
    def assert_mysql_contains(cls, element_text, except_value):
        mysql_db_ = MysqlDB()
        assert mysql_db_.fetch_one(except_value) in element_text

    # 不包含
    @classmethod
    def assert_mysql_not_contains(cls, element_text, except_value):
        mysql_db_ = MysqlDB()
        assert mysql_db_.fetch_one(except_value) not in element_text

    # 布尔判断
    @classmethod
    def assert_mysql_is(cls, element_text, except_value):
        mysql_db_ = MysqlDB()
        assert element_text is mysql_db_.fetch_one(except_value)
