import pandas as pd


def add_title(args, ret):
    ret.append(args.get('步骤名'))


def CleanData(args):
    # pandas 数据清洗
    # rdict = dict_slice(args, 3, len(args))
    # print(rdict)
    # df = pd.DataFrame(rdict)
    # if df[0].isnull:
    #     args = pd.DataFrame.dropna(df)[0].tolist()
    ret = dict_slice(args, 3, len(args))
    add_title(args, ret)
    return ret


def dict_slice(adict, start, end):
    if end < start:
        return adict
    keys = adict.keys()
    rdict = []
    for k in list(keys)[start:end]:
        temp = adict[k]
        if temp is not None and temp != "":
            rdict.append(temp)
    return rdict
