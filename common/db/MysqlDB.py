import os
from os.path import dirname, abspath

import pymysql
from dbutils.pooled_db import PooledDB
import configparser


class MysqlDB(object):

    def __init__(self):
        base_dir = dirname((abspath(__file__)))
        conf = configparser.ConfigParser(allow_no_value=True)
        path = os.path.join(base_dir, "dbconfig.toml")
        conf.read(path, encoding='utf-8')
        self.pool = PooledDB(
            # 使用链接数据库的模块
            # creator=conf.get('database', 'creator').replace("'", ""),
            creator=pymysql,
            # 连接池允许的最大连接数，0和None表示不限制连接数
            # maxconnections=conf.get('database', 'maxconnections'),
            # 初始化时，链接池中至少创建的空闲的链接，0表示不创建
            # mincached=conf.get('database', 'mincached'),
            # 链接池中最多闲置的链接，0和None不限制
            # maxcached=conf.get('database', 'maxcached'),
            # 链接池中最多共享的链接数量，0和None表示全部共享。PS: 无用，因为pymysql和MySQLdb等模块的
            # threadsafety都为1，所有值无论设置为多少，_maxcached永远为0，所以永远是所有链接都共享。
            # maxshared=conf.get('database', 'maxshared'),
            # 连接池中如果没有可用连接后，是否阻塞等待。True，等待；False，不等待然后报错
            # blocking=conf.get('database', 'blocking'),
            # 一个链接最多被重复使用的次数，None表示无限制
            # maxusage=conf.get('database', 'maxusage'),
            # 开始会话前执行的命令列表。如：["set datestyle to ...", "set time zone ..."]
            # setsession=conf.get('database', 'setsession'),
            # ping MySQL服务端，检查是否服务可用。# 如：0 = None = never, 1 = default = whenever it is requested, 2 = when a cursor
            # is created, 4 = when a query is executed, 7 = always
            # ping=conf.get('database', 'ping'),
            host=conf.get('database', 'host').replace("'", ""),
            port=conf.getint('database', 'port'),
            user=conf.get('database', 'user').replace("'", ""),
            password=conf.get('database', 'password').replace("'", ""),
            database=conf.get('database', 'database').replace("'", ""),
            charset=conf.get('database', 'charset').replace("'", "")
        )

    def __new__(cls, *args, **kw):
        """
        启用单例模式
        :param args:
        :param kw:
        :return:
        """
        if not hasattr(cls, '_instance'):
            cls._instance = object.__new__(cls)
        return cls._instance

    def connect(self):
        """
        启动连接
        :return:
        """
        conn = self.pool.connection()
        cursor = conn.cursor(cursor=pymysql.cursors.DictCursor)
        return conn, cursor

    def connect_close(self, conn, cursor):
        """
        关闭连接
        :param conn:
        :param cursor:
        :return:
        """
        cursor.close()
        conn.close()

    def fetch_all(self, sql):
        """
        批量查询
        :param sql:
        :return:
        """
        conn, cursor = self.connect()

        cursor.execute(sql)
        record_list = cursor.fetchall()
        self.connect_close(conn, cursor)

        return record_list

    def fetch_one(self, sql):
        """
        查询单条数据
        :param sql:
        :return:
        """
        conn, cursor = self.connect()
        cursor.execute(sql)
        result = cursor.fetchone()
        self.connect_close(conn, cursor)
        res_str = None
        for i in result.values():
            res_str = i
            break
        return res_str
