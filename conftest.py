import allure
import pytest
from pytest_xlsx.file import XlsxItem

from common.Args.CleanData import CleanData
from common.core.KeyWord import KeyWord


@allure.story(u'@allure.step()')
def pytest_xlsx_run_step(item: XlsxItem):
    wd: KeyWord = item.usefixtures.get("wd")
    step = item.current_step
    key = step['标记']
    args = CleanData(step)
    func = getattr(wd, key)
    func(*args)


@pytest.fixture()
def wd():
    wd = KeyWord()
    yield wd
