# pytest-web-keywords
pytest-web-keywords 是封装pytest的一款关键字驱动框架 
项目演示后台框架： https://gitee.com/R3kkles/refix 
（演示需要先去除验证码，此框架不支持识别验证码）
## 项目结构
![mulu.png](img%2Fmulu.png)
## excel用例样式
![excel.png](img%2Fexcel.png)
### 用例编写
excel名称请以test_XXX.xlsx命名
- 步骤：用例步骤
- 步骤名：用于allure报告标识
- 标记：关键字标识
	- 框架标记
  
    |  名称   |  功能   |
    | --- | --- |
    |  name   |   用例名称   |
    |  mark   |  pytest.mark.xxx   |

	- 自定义标记（具体请看 KeyWord.py）
  
    |  名称   |  功能   |
    | --- | --- |
    |  open_browser  |  打开浏览器  |
    |  get_url  |  打开网址  |
    |  input_content  |  输入框  |
    |  click  |  点击  |
    |  sleep  |  等待  |
    |  switch_frame  |  frame切换  |
    |  assert_func  |  断言  |
    |  close_browser  |  关闭浏览器  |
- <a id=操作方法>操作方法</a>：selenium 输入框查找元素标识
    -  ID
    -  XPATH
    -  LINK_TEXT
    -  PARTIAL_LINK
    -  TAG_NAME
    -  CLASS_NAME
    -  CSS
-  参数：寻找元素具体路径
-  值：selenium sendkey的值
#### 断言
  断言需要用到‘标记’、‘操作方法’、‘参数’、‘值’、‘值2’
  -  标记：固定为 assert_func
  -  操作方法：此处为查找元素，具体请看[操作方法](操作方法)
  -  值：需要使用到的断言方法，具体请看  AssertUtils.py <br>
        数据库断言(目前尚未完善)请注意查询出来的数据需要只返回一个值，返回值转型为str
  -  值2：断言比对的值
## 拓展
如需拓展，只需在KeyWord.py中自定义拓展方法，然后和原本的方法调用一样在excel中调用，如果需要新增参数，只需要在值2后添加值3、值4以此类推，CleanData.py会进行数据清洗，无需担心原本方法兼容问题。
## 开始测试
### 终端输入代码，自动执行excel用例
```
pytest
```
![2024-02-08 12-00-59 -big-original.gif](img%2F2024-02-08%2012-00-59%20-big-original.gif)

### 生成allure报告
```
allure serve report
```
![allure.png](img/allure.png)
断言会自动截图，不需要可在 **assert_func** 方法中注释掉以下代码
```
allure.attach(driver.get_screenshot_as_png(), "截图", allure.attachment_type.PNG)
```
![assert.png](img/assert.png)